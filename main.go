package main

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"io/ioutil"
	"log"
	"torchServeDemo/services"
)

const (
	inferUrl   = "47.101.203.211:7070"
	managerUrl = "47.101.203.211:7071"
)

func main() {
	log.SetFlags(log.Lshortfile | log.Lmicroseconds | log.Ldate)

	//getModelList(0, 10)
	//registerModel()
	//descModelInfo()
	//scaleModel()
	setDefaultModel()
	//unRegisterModel()

	//ping()
	//inferReq()
}

func inferReq() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(inferUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewInferenceAPIsServiceClient(conn)

	// 读取图片
	fileBytes, err := ioutil.ReadFile("kitten_small.jpg")
	if err != nil {
		log.Println("read file fail: ", err)
		return
	}

	predictions, err := client.Predictions(ctx, &services.PredictionsRequest{
		ModelName:    "densenet161",
		ModelVersion: "1.0",
		Input: map[string][]byte{
			"data": fileBytes,
		},
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(predictions)
}

func ping() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(inferUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewInferenceAPIsServiceClient(conn)
	ping, err := client.Ping(ctx, &emptypb.Empty{})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(ping)
}

func scaleModel() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(managerUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewManagementAPIsServiceClient(conn)
	res, err := client.ScaleWorker(ctx, &services.ScaleWorkerRequest{
		ModelName:    "densenet161",
		ModelVersion: "1.0",
		MaxWorker:    3,
		MinWorker:    1,
		NumberGpu:    0,
		Synchronous:  false,
		Timeout:      100,
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(res)
}

func unRegisterModel() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(managerUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewManagementAPIsServiceClient(conn)
	res, err := client.UnregisterModel(ctx, &services.UnregisterModelRequest{
		ModelName:    "densenet162",
		ModelVersion: "1.1",
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(res)
}

func setDefaultModel() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(managerUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewManagementAPIsServiceClient(conn)
	res, err := client.SetDefault(ctx, &services.SetDefaultRequest{
		ModelName:    "densenet161",
		ModelVersion: "1.1",
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(res)
}

func descModelInfo() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(managerUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewManagementAPIsServiceClient(conn)
	res, err := client.DescribeModel(ctx, &services.DescribeModelRequest{
		ModelName:    "densenet161",
		ModelVersion: "1.0",
		Customized:   false,
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(res)

}

func registerModel() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(managerUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewManagementAPIsServiceClient(conn)
	res, err := client.RegisterModel(ctx, &services.RegisterModelRequest{
		InitialWorkers: 2, // initial works
		ModelName:      "densenet161",
		Url:            "densenet161.mar",
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(res)
}

//MARK: - 查询模型列表
func getModelList(page, size int32) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	conn, err := grpc.Dial(managerUrl, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	client := services.NewManagementAPIsServiceClient(conn)
	models, err := client.ListModels(ctx, &services.ListModelsRequest{
		Limit:         size,
		NextPageToken: page,
	})
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(models)

}
