# torch_serve_go_client

引用：

> 三方部署教程 https://www.cnblogs.com/IllidanStormrage/articles/16374941.html
>
> 源码编译 https://github.com/pytorch/serve/blob/master/docs/getting_started.md
>
> grpcAPI https://github.com/pytorch/serve/blob/master/docs/grpc_api.md
>
> serve Docker 操作 https://github.com/pytorch/serve/blob/master/docker/README.md
>
> serve git地址 https://github.com/pytorch/serve
>
> Management API https://pytorch.org/serve/management_api.html#register-a-model
>
> 依赖语言 : java >11.0 , python >= 3.8

 

## 1. 下载模型

官网提供了一个图片识别模型

```bash
wget https://download.pytorch.org/models/densenet161-8d451a50.pth
```

## 2. 转换模型为.mar格式

```bash
# 这里需要安装几个工具
# conda安装
conda install torchserve torch-model-archiver torch-workflow-archiver -c pytorch

# clone serve代码，使用其中的工具脚本进行转换
git clone https://github.com/pytorch/serve.git
cd serve && mkdir model_store
cd ../
mv densenet161-8d451a50.pth ./serve/model_stroe # 把模型移动到serve中的model_store

# 进行模型转换
torch-model-archiver --model-name densenet161 --version 1.0 --model-file ./examples/image_classifier/densenet_161/model.py --serialized-file ./model_store/densenet161-8d451a50.pth --export-path ./model_store --extra-files ./examples/image_classifier/index_to_name.json --handler image_classifier
```

## 3. 拉取官方docker镜像

```bash
# Latest release
docker pull pytorch/torchserve
```

## 4. run image

```bash
# run
docker run --rm --shm-size=1g \		# 共享内存大小，如需更改可以停止容器更改容器config后重启
        --ulimit memlock=-1 \ 		# Maximum locked-in-memory address space
        --ulimit stack=67108864 \ # Linux stack size
        -p8080:8080 \ 						# infer 端口
        -p8081:8081 \ 						# model manager 端口
        -p8082:8082 \ 						# metric apis 端口
        -p7070:7070 \ 						# gRPC 预测端口
        -p7071:7071 \ 						# gRPC 管理模型端口
        --mount type=bind,source=/path/to/model/store,target=/tmp/models pytorch/torchserve <container> --model-store=/tmp/models 
        
# 完整命令
# 在ring-02上 有些端口被占用,下面端口映射进行了更改
# model被存放在了/home/dev/serve/model_store
# container 为torchserve
docker run --rm --shm-size=1g \
        --ulimit memlock=-1 \
        --ulimit stack=67108864 \
        -p9091:8080 \
        -p8084:8081 \
        -p8083:8082 \
        -p7070:7070 \
        -p7071:7071 \
        --mount type=bind,source=/home/dev/serve/model_store,target=/tmp/models pytorch/torchserve torchserve --model-store=/tmp/models 
```



## 5. 模型操作

```bash
# 47.101.203.211
# 查询注册模型
curl "http://127.0.0.1:8084/models"
# 注册模型
curl -X POST "http://127.0.0.1:8084/models?url=模型名称.mar"
# 给模型设置worker
curl -v -X PUT "http://127.0.0.1:8084/models/模型名称?min_worker=2"
# 使用模型进行推理
curl http://127.0.0.1:9091/predictions/densenet161 -T kitten_small.jpg
```



## 6. 其他

```bash
# 停止torch serve(需要提前conda/pip安装相关指令)
touchserve --stop
```





## ⚠注意

> Q: conda install failed
>
> A : 手动创建环境
>
> ```bash
> conda create --name torchserve
> conda update --all
> conda install torchserve torch-model-archiver torch-workflow-archiver -c pytorch
> ```
>
> 
